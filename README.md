## Introduction

This is a simple pet project by a .NET Core application.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)

## What's contained in this project

Directory sctructure:

- docs -- project documentation;
- resources	-- resources needed for project development;
- solution -- files of Visual Studio project;
- tests	-- project tests.


`.gitlab-ci.yml` contains the configuration needed for GitLab
to build code.


Contains two stages: `build`, and `test`. 

```
stages:
    - build
    - test
```

# Branches description